package main

import (
	"database/sql"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
)

// User Struct
type User struct {
	ID        int    `json:"id"`
	Username  string `json:"username"`
	Passsword string `json:"password"`
	Email     string `json:"email"`
}

var dbFilename = "./mockDB.db"

func main() {
	router := mux.NewRouter()

	log.Printf("Connecting to DB: %s", dbFilename)
	db, err := sql.Open("sqlite3", dbFilename)
	if err != nil {
		log.Fatal(err)
	}
	// Close DB connection at the end of main()
	defer db.Close()
	// Test DB connection
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	initializeDatabase(db)
	addMockDataDB(db)

	addMockData()

	router.HandleFunc("/api/users", getUsers).Methods("GET")
	router.HandleFunc("/api/user/{id}", getUser).Methods("GET")
	router.HandleFunc("/api/users", addUser).Methods("POST")
	router.HandleFunc("/api/users/{id}", updateUser).Methods("PUT")
	router.HandleFunc("/api/users/{id}", deleteUser).Methods("DELETE")

	log.Print("Starting http server")
	log.Fatal(http.ListenAndServe(":8000", router))
}
