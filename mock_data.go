package main

import (
	"database/sql"
	"log"
)

var users []User

func addMockData() {
	log.Printf("Adding mock data to array")
	users = append(users, User{ID: 1, Username: "admin", Passsword: "admin", Email: "admin@moneytree.io"})
}

func addMockDataDB(db *sql.DB) {
	log.Printf("Adding mock data to database")
	result, err := db.Exec("INSERT INTO users (username, password, email) VALUES($1, $2, $3)",
		"admin", "adminPass", "admin@moneytree.io")
	if err != nil {
		log.Fatal(err)
	}
	id, _ := result.LastInsertId()
	log.Printf("Added user with ID %d\n", id)
}
