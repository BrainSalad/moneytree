package main

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Get all users
func getUsers(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	json.NewEncoder(response).Encode(users)
}

// Get single user
func getUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	// Get request Params
	reqParams := mux.Vars(request)

	// Get requested ID
	if requestedUserID, err := strconv.Atoi(reqParams["id"]); err == nil {
		// Find user by ID
		for _, user := range users {
			if user.ID == requestedUserID {
				json.NewEncoder(response).Encode(user)
				return
			}
		}
	}
	json.NewEncoder(response).Encode(&User{})
}

// Add new user
func addUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	user := User{}
	_ = json.NewDecoder(request.Body).Decode(&user)
	users = append(users, user)
	json.NewEncoder(response).Encode(user)
}

// Update user
func updateUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	// Get request params
	reqParams := mux.Vars(request)

	newUser := User{}
	currentUser := &User{}

	// Get requested ID
	if requestedUserID, err := strconv.Atoi(reqParams["id"]); err == nil {
		// Find user by ID
		for index := range users {
			if users[index].ID == requestedUserID {
				json.NewDecoder(request.Body).Decode(&newUser)
				users[index] = newUser
				currentUser = &users[index]
				break
			}
		}
	}
	json.NewEncoder(response).Encode(&currentUser)
}

// Delete user
func deleteUser(response http.ResponseWriter, request *http.Request) {

}
