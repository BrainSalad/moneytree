package main

import (
	"database/sql"
	"log"
)

func createTables(db *sql.DB) {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS users (
		id 			INTEGER PRIMARY KEY AUTOINCREMENT,
		username	TEXT,
		password	TEXT,
		email 		TEXT
	) `)
	if err != nil {
		log.Fatal(err)
	}
}

func initializeDatabase(db *sql.DB) {
	log.Printf("Initializing database")
	createTables(db)
}
